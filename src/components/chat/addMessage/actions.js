import { ADD_MESSAGE_UPDATE_TEXT, ADD_MESSAGE_RESET_TEXT } from "./actionTypes";

export const updateText = (text) => ({
    type: ADD_MESSAGE_UPDATE_TEXT,
    payload: {
        text
    }
})

export const resetText = () => ({
    type: ADD_MESSAGE_RESET_TEXT
})