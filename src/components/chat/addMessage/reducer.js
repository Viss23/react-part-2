import { ADD_MESSAGE_UPDATE_TEXT, ADD_MESSAGE_RESET_TEXT } from "./actionTypes";

const initialState = {
    text: ''
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_MESSAGE_UPDATE_TEXT: {
            const { text } = action.payload;
            return {
                ...state,
                text: text
            };
        }

        case ADD_MESSAGE_RESET_TEXT: {
            return {
                ...state,
                text: ''
            };
        }

        default:
            return state;
    }
}