import React from "react";
import { connect } from 'react-redux';
import * as actions from './actions';
import { updateMessage } from '../actions';
import style from '../../../mystyle.module.css';

class EditModal extends React.Component {
  constructor(props) {
    super(props);
    this.onTextChange = this.onTextChange.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  onTextChange(text) {
    this.props.updateText(text);
  }

  onCancel() {
    this.props.dropCurrentUserId();
    this.props.resetText();
    this.props.hidePage();
  }

  onSave() {
    this.props.updateMessage(this.props.userId, this.props.text);
    this.props.resetText();
    this.props.hidePage();
  }

  getPageContent() {

    return (
      <div className="modal" style={{ display: "block" }} tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content" style={{ padding: "5px" }}>
            <div className="modal-header">
              <h5 className="modal-title">Edit message</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.onCancel}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <textarea value={this.props.text} className={style.textAreaSendModal} onChange={(e) => this.onTextChange(e.target.value)}></textarea>
            </div>
            <div className="modal-footer">
              <button className="btn btn-secondary" onClick={this.onCancel}>Cancel</button>
              <button className="btn btn-primary" onClick={this.onSave}>Save</button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    const isShown = this.props.isShown;
    return isShown ? this.getPageContent() : null;
  }
}

const mapStateToProps = (state) => {
  return {
    isShown: state.editModal.isShown,
    userId: state.editModal.userId,
    text: state.editModal.text
  }
};

const mapDispatchToProps = {
  ...actions,
  updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);