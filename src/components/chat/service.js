const getNewId = () => Math.random().toString(16).slice(2);
const getTime = () => new Date().toISOString();
const sortObjByCreatedAt = (obj) => {
    return obj.sort((a, b) => {
        return new Date(b.createdAt) - new Date(a.createdAt)
    })
}

export default {
    getNewId,
    getTime,
    sortObjByCreatedAt
};