import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, GET_DATA_DONE } from "./actionTypes";
import mockData from "../../data";
import service from './service';


const initialState = {
  loading: true,
  myUserName: mockData.currentUser.userName,
  myUserId: mockData.currentUser.userId,
  myAvatar: mockData.currentUser.avatar,
  chatName: mockData.headerInfo.chatName,
  participantsAmount: mockData.headerInfo.participantsAmount,
}

export default function (state = initialState, action) {
  switch (action.type) {

    case GET_DATA_DONE:
      return { ...state, loading: false, data: service.sortObjByCreatedAt(action.payload.data) };

    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const filteredMessages = state.data.filter(message => {
        return message.id !== id
      });

      return {
        ...state,
        data: filteredMessages
      }
    }

    case UPDATE_MESSAGE: {
      const { id, text } = action.payload;
      const updateMessages = state.data.map(message => {
        if (message.id === id) {
          return {
            ...message,
            text
          }
        } else {
          return message;
        }
      });
      return {
        ...state,
        data: updateMessages
      }
    }

    case ADD_MESSAGE: {
      const { id, createdAt, data } = action.payload;
      const newMessage = { id, createdAt, ...data };
      const oldData = state.data;
      const updatedData = [...oldData, newMessage];
      const sortedUpdatedData = service.sortObjByCreatedAt(updatedData);
      return {
        ...state,
        data: sortedUpdatedData
      }
    }

    default:
      return state
  }

}
