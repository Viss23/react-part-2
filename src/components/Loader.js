import React from 'react';
import style from '../mystyle.module.css';


class Loader extends React.Component {

  render() {

    return (
      <div className={style.loader}>Loading...</div>
    )
  }
}

export default Loader;